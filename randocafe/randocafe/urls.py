"""
URL configuration for randocafe project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView

urlpatterns = [
    path("randocafe/api/v1/", include("catalog.api")),
    path("randocafe/admin/", admin.site.urls),
    path("randocafe/catalog/", include("catalog.urls")),
    path("", RedirectView.as_view(url="randocafe/catalog/")),
    path("randocafe/accounts/", include("django.contrib.auth.urls")),
]
