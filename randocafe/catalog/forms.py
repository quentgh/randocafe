import datetime

from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _

from .models import MyUser, Trip, TripInstance


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = MyUser
        fields = ("email",)


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = MyUser
        fields = ("email",)


class ExtendTripForm(forms.Form):
    extend_date = forms.DateField(
        help_text="Enter a date between now and 2 weeks (default = 1)."
    )

    def clean_extend_date(self):
        data = self.cleaned_data["extend_date"]

        # Check if a date is not in the past.
        if data < datetime.date.today():
            raise ValidationError(_("Invalid date - renewal in past"))

        # Check if a date is in the allowed range (+2 weeks from today).
        if data > datetime.date.today() + datetime.timedelta(weeks=2):
            raise ValidationError(_("Invalid date - renewal more than 2 weeks ahead"))

        # Remember to always return the cleaned data.
        return data


class ExtendTripModelForm(ModelForm):
    def clean_end_date(self):
        data = self.cleaned_data["end_date"]

        # Check if a date is not in the past.
        if data < datetime.date.today():
            raise ValidationError(_("Invalid date - renewal in past"))

        # Check if a date is in the allowed range (+2 weeks from today).
        if data > datetime.date.today() + datetime.timedelta(weeks=2):
            raise ValidationError(_("Invalid date - extend more than 2 weeks ahead"))

        # Remember to always return the cleaned data.
        return data

    class Meta:
        model = TripInstance
        fields = ["end_date"]
        labels = {"end_date": _("Extend date")}
        help_texts = {
            "end_date": _("Enter a date between now and 2 weeks (default 1).")
        }


class TripInstanceModelForm(ModelForm):
    def clean_end_date(self):
        data = self.cleaned_data["end_date"]
        data_start = self.cleaned_data["star_date"]

        # Check if a date is not in the past.
        if data < datetime.date.today():
            raise ValidationError(
                _("Date invalide - ne peut pas être antérieure à aujourd'hui.")
            )

        # Check if a date isn't previous to the start date
        if data < data_start:
            raise ValidationError(
                _(
                    "Date invalide - ne peut pas être antérieure à la date de début de réservation."
                )
            )

        # Remember to always return the cleaned data.
        return data

    class Meta:
        model = TripInstance
        fields = ["end_date"]


class TripDeleteForm(ModelForm):
    class Meta:
        model = Trip
        fields = ("is_deleted",)
