import uuid
from datetime import date

from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db import models as gis_models
from django.db import models
from django.urls import reverse

from .managers import SoftDeleteManager, UserManager


class SoftDeleteModel(models.Model):
    is_deleted = models.BooleanField(default=False)
    objects = SoftDeleteManager()
    all_objects = models.Manager()

    def soft_delete(self):
        self.is_deleted = True
        self.save()

    def restore(self):
        self.is_deleted = False
        self.save()

    class Meta:
        abstract = True


class MyUser(AbstractUser):
    """Model representing a custom user"""

    username = None
    email = models.EmailField(max_length=100, unique=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self) -> str:
        """String for representing the model object"""
        return f"{self.email}"


class Author(models.Model):
    """Model representing a trip author"""

    user = models.OneToOneField(MyUser, on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ["user__last_name"]

    def get_absolute_url(self):
        """Return the url to access a particular Author instance"""
        return reverse("author-details", args=[str(self.id)])

    def __str__(self) -> str:
        """String for representing the model object"""
        return f"{self.user.first_name}, {self.user.last_name}"


class Client(models.Model):
    """Model representing a client"""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text="ID unique")

    first_name = models.CharField(max_length=200, help_text="Prénom")
    last_name = models.CharField(max_length=200, help_text="Nom")

    date_of_birth = models.DateField(help_text="Format date : [aaaa-mm-jj]")

    # user = models.OneToOneField(MyUser, on_delete=models.SET_NULL, null=True)

    PARTICIPANT_STATUS = (("l", "leader"), ("f", "follower"))

    status = models.CharField(
        max_length=1,
        choices=PARTICIPANT_STATUS,
        blank=True,
        default="f",
        help_text="Statut du participant",
    )

    def get_absolute_url(self):
        """Return the url to access a particular Client instance"""
        return reverse("client-detail", args=[str(self.id)])

    def __str__(self) -> str:
        """String for representing the model object"""
        return f"{self.first_name}, {self.last_name}"


class Type(models.Model):
    """Model representing a hiking type"""

    name = models.CharField(max_length=200)

    def __str__(self) -> str:
        return self.name


'''
class Level(models.Model):
    """Model representing a hiking level"""

    LEVEL_NAME = (("e", "easy"), ("m", "medium"), ("d", "difficult"))

    name = models.CharField(
        max_length=1,
        choices=LEVEL_NAME,
        blank=True,
        default="e",
        help_text="Hiking level",
    )

    def __str__(self) -> str:
        return self.name
'''


class Coffee(models.Model):
    """Model representing a coffee"""

    name = models.CharField(max_length=200)

    def __str__(self) -> str:
        return self.name


class Marker(models.Model):
    """Model representing a marker"""

    name = models.CharField(max_length=255)
    location = gis_models.PointField()

    def get_absolute_url(self):
        """Return the url to access a particular marker instance"""
        return reverse("marker-detail", args=[str(self.id)])

    def __str__(self) -> str:
        return self.name


class Roaster(models.Model):
    """Model representing a roaster"""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, help_text="ID unique")

    name = models.CharField(max_length=200, help_text="Nom")
    address = models.CharField(max_length=300, help_text="Adresse du torréfacteur")
    marker = models.ForeignKey(
        Marker,
        on_delete=models.SET_NULL,
        null=True,
        help_text="Sélectionner un géo-marqueur",
    )
    description = models.TextField(
        max_length=1000, help_text="Entrer une description du torréfacteur"
    )

    coffee = models.ManyToManyField(
        Coffee, help_text="Sélectionner les cafés diponibles"
    )

    def get_absolute_url(self):
        """Return the url to access a particular Roaster instance"""
        return reverse("roaster-detail", args=[str(self.id)])

    def __str__(self) -> str:
        """String for representing the model object"""
        return f"{self.name}"


class Hiking(models.Model):
    """Model representing a hiking"""

    name = models.CharField(max_length=200, help_text="Nom")
    description = models.TextField(
        max_length=1000, help_text="Donner une description de la randonnée"
    )

    # level = models.ForeignKey(Level, on_delete=models.SET_NULL, null=True)
    marker = models.ForeignKey(
        Marker,
        on_delete=models.SET_NULL,
        null=True,
        help_text="Attribuer un géo-marqueur",
    )
    # type = models.ManyToManyField(Type, help_text="Select type(s) for this hiking")
    type = models.ManyToManyField(
        Type,
        help_text='Sélectionner un ou des types de randonnée, i.e. "Ville", "Forêt", "Mer" ...',
    )

    LEVEL_NAME = (("e", "easy"), ("m", "medium"), ("d", "difficult"))

    level = models.CharField(
        max_length=1,
        choices=LEVEL_NAME,
        blank=True,
        default="e",
        help_text='Niveau de difficulté de la randonnée, i.e. "Facile", "Moyen", "Difficile"',
    )

    roaster = models.OneToOneField(
        Roaster, on_delete=models.SET_NULL, null=True, help_text="Torréfacteur"
    )

    '''
    def display_level(self):
        """Create a string for the Level."""
        return ", ".join(level.name for level in self.level.all()[:3])

    display_level.short_description = "Level"
    '''

    def get_absolute_url(self):
        """Returns the URL to access a detail record for this hiking."""
        return reverse("hiking-detail", args=[str(self.id)])

    def display_type(self):
        """Create a string for the type."""
        return ", ".join(type.name for type in self.type.all()[:3])

    display_type.short_description = "Type"

    def __str__(self) -> str:
        """String for representing the Model object."""
        return self.name


class Trip(SoftDeleteModel):
    """Model representing a trip"""

    name = models.CharField(max_length=150, help_text="Nom")
    created = models.DateTimeField(auto_now=True)
    description = models.TextField(
        max_length=1000, help_text="Entrer une courte description du trip"
    )

    author = models.ForeignKey(
        Author, on_delete=models.SET_NULL, null=True, help_text="Auteur de ce trip"
    )
    hiking = models.ForeignKey(
        Hiking,
        on_delete=models.SET_NULL,
        null=True,
        help_text="Sélectionner une randonnée pour ce trip",
    )

    class Meta:
        ordering = ["name", "created"]

    def get_absolute_url(self):
        """Returns the URL to access a detail record for this trip."""
        return reverse("trip-detail", args=[str(self.id)])

    def __str__(self) -> str:
        """String for representing the Model object."""
        return self.name


class TripInstance(models.Model):
    """Model representing an instance of a trip"""

    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        help_text="ID unique pour une instance spécipfique d'un trip",
    )

    clients = models.ManyToManyField(
        Client, help_text="Sélectionner une liste de clients"
    )
    created = models.DateTimeField(auto_now=True)
    start_date = models.DateField(help_text="Format date : [aaaa-mm-jj]")
    end_date = models.DateField(help_text="Format date : [aaaa-mm-jj]")

    trip = models.ForeignKey(
        Trip,
        on_delete=models.RESTRICT,
        null=True,
        help_text="Sélectionner un trip existant",
    )

    owner = models.ForeignKey(
        MyUser,
        on_delete=models.RESTRICT,
        null=True,
        help_text="Sélectionner un propriétaire pour cette réservation",
    )

    BOOKING_STATUS = (
        ("b", "booked"),
        ("f", "finished"),
        ("i", "in progress"),
        ("p", "pending"),
    )

    status = models.CharField(
        max_length=1,
        choices=BOOKING_STATUS,
        blank=True,
        default="p",
        help_text="Status de la réservation",
    )

    @property
    def passed_deadline(self):
        """Determines if the trip have passed the deadline"""
        return bool(self.end_date and date.today() > self.end_date)

    def get_absolute_url(self):
        """Returns the URL to access a detail record for this trip instance."""
        return reverse("tripinstance-detail", args=[str(self.id)])

    def __str__(self):
        """String for representing the Model object."""
        return f"{self.id} ({self.trip.name})"

    class Meta:
        ordering = ["created"]
        permissions = (
            ("can_list_all_instances", "List all trip instances"),
            ("can_list_own_instances", "List own trip instance"),
            ("can_list_pending_instance", "List pending trip instance"),
            ("can_list_inprogress_instance", "List in progress trip instance"),
            ("can_list_booked_instance", "List booked trip instance"),
            ("can_list_archived_instance", "List archived trip instance"),
        )
