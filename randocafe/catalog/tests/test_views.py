import datetime

from catalog.models import TripInstance
from django.contrib.auth import get_user_model
from django.test import TestCase

User = get_user_model()


class AllManagedTripsListViewTest(TestCase):
    def setUp(self) -> None:
        # Create a client

        # Create a created field
        test_created = datetime(2023, 10, 25, 10, 30, 10, 277123)

        # Create a TripInstance
        TripInstance.objects.create(
            id="123456789abcabc123456789abcabc99", created=test_created
        )
