import datetime
import logging
from typing import Any

from catalog.forms import ExtendTripModelForm, TripDeleteForm, TripInstanceModelForm

# from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

# from rest_framework.response import Response
# from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST
# from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenObtainPairView

from .models import Author, Client, Hiking, Marker, Roaster, Trip, TripInstance
from .serializers import TokenObtainPairSerializer

logger = logging.getLogger(__name__)


@login_required
def profile(request):
    """View function for profile page."""
    logger.info(f"The user {request.user} is connected")

    return render(request, "catalog/profile.html")


@login_required
def done(request):
    """View function for done page."""

    return render(request, "catalog/done.html")


@login_required
def index(request):
    """View function for home page of site."""

    request.session["theme"] = ""

    # Generate counts of some of the main objects
    num_authors = Author.objects.all().count()  # .all() is applied by default
    num_client = Client.objects.count()
    num_trips = Trip.objects.count()
    num_tripInstances = TripInstance.objects.count()

    # Booked trip (status = 'b')
    num_tripInstances_booked = TripInstance.objects.filter(status__exact="b").count()

    # In progress trip (status = 'i')
    num_tripInstances_inProgress = TripInstance.objects.filter(
        status__exact="i"
    ).count()

    # Pending trip (status = 'f')
    num_tripInstances_finished = TripInstance.objects.filter(status__exact="f").count()

    # Pending trip (status = 'p')
    num_tripInstances_pending = TripInstance.objects.filter(status__exact="p").count()

    context = {
        "num_client": num_client,
        "num_authors": num_authors,
        "num_trips": num_trips,
        "num_tripInstances": num_tripInstances,
        "num_tripInstances_inProgress": num_tripInstances_inProgress,
        "num_tripInstances_booked": num_tripInstances_booked,
        "num_tripInstances_pending": num_tripInstances_pending,
        "num_tripInstances_finished": num_tripInstances_finished,
    }

    # Render the HTML template index.html with the data in the context variable
    return render(request, "index.html", context=context)


@login_required
@permission_required("catalog.can_list_all_instances", raise_exception=True)
def extend_trip_employee(request, pk):
    """View function for extend a specific TripInstance by a Employee."""

    trip_instance = get_object_or_404(TripInstance, pk=pk)

    if request.method == "POST":
        form = ExtendTripModelForm(request.POST)

        if form.is_valid():
            trip_instance.end_date = form.cleaned_data["end_date"]
            trip_instance.save()

            return HttpResponseRedirect(reverse("managed-trips"))

    else:
        proposed_extend_date = datetime.date.today() + datetime.timedelta(weeks=1)
        form = ExtendTripModelForm(initial={"extend_date": proposed_extend_date})

    context = {
        "form": form,
        "trip_instance": trip_instance,
    }

    return render(request, "catalog/extend_trip_employee.html", context)


""" Trip CRUD """


class TripListView(LoginRequiredMixin, generic.ListView):
    model = Trip
    # paginate_by = 5

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        trips = Trip.all_objects.filter(is_deleted=False)
        # context = super(TripListView, self).get_context_data(**kwargs)
        context = {
            "trips": trips,
        }
        return context


class TripDetailView(LoginRequiredMixin, generic.DetailView):
    model = Trip

    def trip_detail_view(request, primary_key):
        trip = get_object_or_404(Trip, pk=primary_key)
        context = {"trip": trip}
        return render(request, "catalog/trip_detail.html", context)


class TripCreate(LoginRequiredMixin, CreateView):
    model = Trip
    fields = ["name", "description", "author", "hiking"]
    # initial= ''
    success_url = reverse_lazy("done")


class TripUpdate(LoginRequiredMixin, UpdateView):
    model = Trip
    fields = ["name", "description", "hiking"]
    success_url = reverse_lazy("done")


"""
class TripDelete(LoginRequiredMixin, DeleteView):
    model = Trip
    success_url = reverse_lazy("trip-soft-delete")
"""


@login_required
def tripSoftDelete(request, pk):
    """View function for Trip soft delete page"""

    trip = get_object_or_404(Trip, pk=pk)

    if request.method == "POST":
        form = TripDeleteForm(request.POST)

        if form.is_valid():
            trip.soft_delete()

            return HttpResponseRedirect(reverse("done"))

    else:
        form = TripDeleteForm(initial={"is_deleted": False})

    context = {
        "form": form,
        "trip": trip,
    }

    return render(request, "catalog/trip_confirm_delete.html", context)


""" Trip Instance CRUD """


class TripInstanceListView(LoginRequiredMixin, generic.ListView):
    model = TripInstance
    paginate_by = 5

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super(TripInstanceListView, self).get_context_data(**kwargs)
        return context


class TripInstanceDetailView(LoginRequiredMixin, generic.DetailView):
    model = TripInstance

    def trip_instance_detail_view(request, primary_key):
        tripInstance = get_object_or_404(TripInstance, pk=primary_key)
        context = {"tripInstance": tripInstance}
        return render(request, "catalog/tripinstance_detail.html", context)


class TripInstanceCreate(LoginRequiredMixin, CreateView):
    model = TripInstance
    fields = ["id", "clients", "start_date", "end_date", "trip", "owner", "status"]

    def check_end_date(request, pk):
        trip_instance = get_object_or_404(TripInstance, pk=pk)
        start_date = trip_instance.start_date
        print("START DATE : ", start_date)
        # If this is a POST request then process the Form data
        if request.method == "POST":
            # Create a form instance and populate it with data from the request (binding):
            form = TripInstanceModelForm(request.POST)

            # Check if the form is valid:
            if form.is_valid():
                # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
                trip_instance.end_date = form.cleaned_data["end_date"]
                trip_instance.save()

                # redirect to a new URL:
                return HttpResponseRedirect(reverse("done"))

        # If this is a GET (or any other method) create the default form.
        else:
            proposed_end_date = start_date + datetime.timedelta(weeks=1)
            form = TripInstanceModelForm(initial={"end_date": proposed_end_date})

        context = {
            "form": form,
            "trip_instance": trip_instance,
        }

        return render(request, "tripinstance-create", context)


class TripInstanceUpdate(LoginRequiredMixin, UpdateView):
    model = TripInstance
    fields = ["clients", "start_date", "end_date", "trip", "owner", "status"]
    success_url = reverse_lazy("done")


class TripInstanceDelete(LoginRequiredMixin, DeleteView):
    model = TripInstance
    success_url = reverse_lazy("tripinstances")


""" Client CRUD """


class ClientListView(LoginRequiredMixin, generic.ListView):
    model = Client
    paginate_by = 5

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super(ClientListView, self).get_context_data(**kwargs)
        return context


class ClientDetailView(LoginRequiredMixin, generic.DetailView):
    # login_url = '/api/v1/randocafe/accounts/login/'
    # redirect_field_name = "redirect_to"

    model = Client

    def client_detail_view(request, primary_key):
        client = get_object_or_404(Client, pk=primary_key)
        context = {"client": client}
        return render(request, "catalog/client_detail.html", context)


class ClientCreate(LoginRequiredMixin, CreateView):
    model = Client
    fields = ["id", "first_name", "last_name", "date_of_birth", "status"]
    success_url = reverse_lazy("done")


class ClientUpdate(LoginRequiredMixin, UpdateView):
    model = Client
    fields = ["first_name", "last_name", "date_of_birth", "status"]
    success_url = reverse_lazy("done")


class ClientDelete(LoginRequiredMixin, DeleteView):
    model = Client
    success_url = reverse_lazy("clients")


""" Hiking CRUD """


class HikingListView(LoginRequiredMixin, generic.ListView):
    model = Hiking

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super(HikingListView, self).get_context_data(**kwargs)
        return context


class HikingDetailView(LoginRequiredMixin, generic.DetailView):
    model = Hiking

    def hiking_detail_view(request, primary_key):
        hiking = get_object_or_404(Hiking, pk=primary_key)
        context = {"hiking": hiking}
        return render(request, "catalog/hiking_detail.html", context)


class HikingCreate(LoginRequiredMixin, CreateView):
    model = Hiking
    fields = ["name", "description", "marker", "type", "level", "roaster"]
    success_url = reverse_lazy("done")


class HikingUpdate(LoginRequiredMixin, UpdateView):
    model = Hiking
    fields = ["name", "description", "marker", "type", "level", "roaster"]
    success_url = reverse_lazy("done")


class HikingDelete(LoginRequiredMixin, DeleteView):
    model = Hiking
    success_url = reverse_lazy("hikings")


""" Roaster CRUD """


class RoasterListView(LoginRequiredMixin, generic.ListView):
    model = Roaster

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super(RoasterListView, self).get_context_data(**kwargs)
        return context


class RoasterDetailView(LoginRequiredMixin, generic.DetailView):
    model = Roaster

    def roaster_detail_view(request, primary_key):
        roaster = get_object_or_404(Roaster, pk=primary_key)
        context = {"roaster": roaster}
        return render(request, "catalog/roaster_detail.html", context)


class RoasterCreate(LoginRequiredMixin, CreateView):
    model = Roaster
    fields = ["id", "name", "address", "marker", "description", "coffee"]
    success_url = reverse_lazy("done")


class RoasterUpdate(LoginRequiredMixin, UpdateView):
    model = Roaster
    fields = ["name", "address", "marker", "description", "coffee"]
    success_url = reverse_lazy("done")


class RoasterDelete(LoginRequiredMixin, DeleteView):
    model = Roaster
    success_url = reverse_lazy("roasters")


""" Other View classes """


class BookedTripByUserListView(
    LoginRequiredMixin, PermissionRequiredMixin, generic.ListView
):

    """Generic class-based view listing booked trip to current user."""

    model = TripInstance
    template_name = "catalog/tripinstance_list_client_user.html"
    paginate_by = 10
    permission_required = "catalog.can_list_own_instances"

    def get_queryset(self):
        return (
            TripInstance.objects.filter(owner=self.request.user)
            .filter(status__exact="b")
            .order_by("start_date")
        )


class AllManagedTripsListView(
    LoginRequiredMixin, PermissionRequiredMixin, generic.ListView
):
    """Generic class-based view listing all managed trips of the agency."""

    model = TripInstance
    template_name = "catalog/tripinstance_all_list_managed_user.html"
    paginate_by = 5
    permission_required = "catalog.can_list_all_instances"

    def get_queryset(self):
        return TripInstance.objects.all().order_by("created")


class PendingTripsListView(
    LoginRequiredMixin, PermissionRequiredMixin, generic.ListView
):

    """Generic class-based view listing pending trips."""

    model = TripInstance
    template_name = "catalog/tripinstance_list_pending.html"
    paginate_by = 5
    permission_required = "catalog.can_list_pending_instance"

    def get_queryset(self):
        return TripInstance.objects.filter(status__exact="p").order_by("start_date")


class InProgressTripsListView(
    LoginRequiredMixin, PermissionRequiredMixin, generic.ListView
):

    """Generic class-based view listing in progress trips."""

    model = TripInstance
    template_name = "catalog/tripinstance_list_inprogress.html"
    paginate_by = 5
    permission_required = "catalog.can_list_inprogress_instance"

    def get_queryset(self):
        return TripInstance.objects.filter(status__exact="i").order_by("start_date")


class BookedTripsListView(
    LoginRequiredMixin, PermissionRequiredMixin, generic.ListView
):

    """Generic class-based view listing booked trips."""

    model = TripInstance
    template_name = "catalog/tripinstance_list_booked.html"
    paginate_by = 5
    permission_required = "catalog.can_list_booked_instance"

    def get_queryset(self):
        return TripInstance.objects.filter(status__exact="b").order_by("start_date")


class ArchivedTripsListView(
    LoginRequiredMixin, PermissionRequiredMixin, generic.ListView
):

    """Generic class-based view listing archived trips."""

    model = TripInstance
    template_name = "catalog/tripinstance_list_archived.html"
    paginate_by = 5
    permission_required = "catalog.can_list_archived_instance"

    def get_queryset(self):
        return TripInstance.objects.filter(status__exact="f").order_by("owner")


""" TemplateView for testing the map view with my markers"""


class MarkersMapView(LoginRequiredMixin, TemplateView):
    model = Marker
    template_name = "catalog/map.html"


class MarkerListView(LoginRequiredMixin, generic.ListView):
    model = Marker

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super(MarkerListView, self).get_context_data(**kwargs)
        return context


class MarkerDetailView(LoginRequiredMixin, generic.DetailView):
    model = Marker

    def marker_detail_view(request, primary_key):
        marker = get_object_or_404(Marker, pk=primary_key)
        context = {"marker": marker}
        return render(request, "catalog/marker_detail.html", context)


"""
class APIRegisterView(LoginRequiredMixin, APIView):
    http_method_names = ['post']

    def post(self, *args, **kwargs):
        print("dans le post api register")
        serializer = APIUserSerializer(data=self.request.data)
        if serializer.is_valid():
            get_user_model().objects.create_user(**serializer.validated_data)
            return Response(status=HTTP_201_CREATED)
        return Response(status=HTTP_400_BAD_REQUEST, data={'errors': serializer.errors})
"""


class EmailTokenObtainPairView(TokenObtainPairView):
    serializer_class = TokenObtainPairSerializer
