# from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.gis import admin

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import (
    Author,
    Client,
    Coffee,
    Hiking,
    Marker,
    MyUser,
    Roaster,
    Trip,
    TripInstance,
    Type,
)


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = MyUser
    list_display = (
        "email",
        "is_staff",
        "get_groups",
        "is_active",
    )
    list_filter = (
        "is_staff",
        "groups",
        "is_active",
    )
    fieldsets = (
        (None, {"fields": ("email", "password", "last_name", "first_name")}),
        (
            "Permissions",
            {"fields": ("is_staff", "is_active", "groups", "user_permissions")},
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "email",
                    "password1",
                    "password2",
                    "is_staff",
                    "is_active",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
    )
    search_fields = ("email",)
    ordering = ("email",)

    def get_groups(self, obj):
        return obj.groups.values_list("name", flat=True).get()

    get_groups.short_description = "Groups"


admin.site.register(MyUser, CustomUserAdmin)
# admin.site.register(Author)
admin.site.register(Client)
# admin.site.register(Type)
admin.site.register(Coffee)
admin.site.register(Roaster)
# admin.site.register(Marker)
admin.site.register(Hiking)
# dmin.site.register(Trip)
# admin.site.register(TripInstance)


@admin.register(Marker)
class MarkerAdmin(admin.GISModelAdmin):
    model = Marker
    list_display = (
        "name",
        "location",
    )

    list_filter = ("name",)

    search_fields = ("name",)
    ordering = ("name",)


class TripsInline(admin.TabularInline):
    model = Trip
    extra = 0


class TripsInstanceInline(admin.TabularInline):
    model = TripInstance
    extra = 0


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    inlines = [TripsInline]


@admin.register(Trip)
class TripAdmin(admin.ModelAdmin):
    list_display = ("name", "created")

    list_filter = ("name",)

    inlines = [TripsInstanceInline]


@admin.register(TripInstance)
class TripInstanceAdmin(admin.ModelAdmin):
    list_display = ("trip", "id", "owner", "created", "status")

    list_filter = ("status", "created")

    fieldsets = (
        (None, {"fields": ("trip", "id", "owner")}),
        ("Status", {"fields": ("status", "start_date", "end_date", "clients")}),
    )


@admin.register(Type)
class TypeAdmin(admin.ModelAdmin):
    pass
