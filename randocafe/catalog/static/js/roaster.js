const copy =
  "&copy; <a href='https://www.openstreetmap.org/copyright'>OpenStreetMap</a> contributors";
const url = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
const layer = L.tileLayer(url, { attribution: copy });
const map = L.map("map", { layers: [layer], minZoom: 5 });
const data = document.currentScript.dataset;
const roasterMarker = data.marker;
const token = localStorage.getItem("token");
const bearerToken = `Bearer ${token}`;

map
  .locate()
  .on("locationfound", (e) => map.setView(e.latlng, 8))
  .on("locationerror", () => map.setView([47.393701, 0.690079], 4));

async function load_markers() {
    const markers_url = `/randocafe/api/v1/markers/?in_bbox=${map.getBounds().toBBoxString()}`;
    const response = await fetch(markers_url,
      { headers: {Authorization: bearerToken}});
    const geojson = await response.json();
    return geojson;
  }

async function render_marker() {
    const markers = await load_markers();
    L.geoJSON(markers, {
        filter : function(feature, layer){
            if (feature.properties.name == roasterMarker){
                return feature.properties.name;
            }
        }
    }).bindPopup((layer) => layer.feature.properties.name)
      .addTo(map);
  }

  map.on("moveend", render_marker);

//map.setView([48.8567, 2.3508], 5);
//map.setView([48.85659479478178, 2.3476120214369334], 8);
