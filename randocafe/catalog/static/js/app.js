// SIDEBAR TOGGLE

let sideBarOpen = false;
let sideBar = document.getElementById("sidebar");
let menuBtn = document.getElementById("menu-btn");
let menuClose = document.getElementById("menu-close");

menuBtn.addEventListener("click", () => {
  if (!sideBarOpen) {
    sideBar.classList.add("sideBar-responsive");
    sideBarOpen = true;
  }
});

menuClose.addEventListener("click", () => {
  if (sideBarOpen) {
    sideBar.classList.remove("sideBar-responsive");
    sideBarOpen = false;
  }
});

// DARK MODE
const gridContainer = document.querySelector(".grid-container");
const mainContainer = document.querySelector(".main-container");
const btn = document.querySelector(".btn");
const switchBox = document.querySelector(".switch");
const icone = document.querySelector("i.fas.fa-moon");
const footer = document.querySelector("footer")

//btn.addEventListener("click", () => {
//  gridContainer.classList.toggle("dark");
//  mainContainer.classList.toggle("dark");
//  footer.classList.toggle("dark");
//});

let theme = sessionStorage.getItem("theme");
sessionStorage.setItem("theme", "light");

let isDark = false;

if (theme != 'dark'){
  //console.log("is not dark");
  sessionStorage.setItem("theme", "light");
  gridContainer.classList.remove("dark");
  mainContainer.classList.remove("dark");
  footer.classList.remove("dark");
  btn.classList.remove("btn-change");
  switchBox.classList.remove("light");
  icone.classList.remove("icone-change");
  icone.classList.remove("fa-sun");
  isDark = false;
} else {
  //console.log("is dark");
  sessionStorage.setItem("theme", "dark");
  gridContainer.classList.add("dark");
  mainContainer.classList.add("dark");
  footer.classList.add("dark");
  btn.classList.add("btn-change");
  switchBox.classList.add("light");
  icone.classList.add("icone-change");
  icone.classList.add("fa-sun");
  isDark = true;
}

btn.addEventListener("click", () => {
  gridContainer.classList.toggle("dark");
  mainContainer.classList.toggle("dark");
  footer.classList.toggle("dark");
  btn.classList.toggle("btn-change");
  switchBox.classList.toggle("light");
  icone.classList.toggle("icone-change");
  icone.classList.toggle("fa-sun");
  if(!isDark){
    sessionStorage.setItem("theme", "dark");
  } else {
    sessionStorage.setItem("theme", "light");
  }
});
