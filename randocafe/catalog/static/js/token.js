const getTokenUrl = "https://127.0.0.1:8443/randocafe/catalog/token/obtain/";

function getCookie(name) {
    const cookies = document.cookie.split('; ')
    const value = cookies
        .find(c => c.startsWith(name + "="))
        ?.split('=')[1]
    if (value === undefined) {
        return null
    }
    return decodeURIComponent(value)
}

const getActiveCSRF = getCookie("csrftoken");

document.querySelector("#api-call").addEventListener("click", event => {
    event.preventDefault();
    let formData = new FormData();
    formData.append('email', document.querySelector("#email").value);
    formData.append('password', document.querySelector("#password").value);
    const request = new Request(getTokenUrl, {
        method: 'POST',
        body: formData,
        headers: {'X-CSRFToken': getActiveCSRF}
    });
    fetch(request)
        .then(response => response.json())
        .then(result => {
            const resultMsg = document.querySelector("#msg");

            for (const [key, value] of Object.entries(result)) {
                if (key == "access") {
                    const token = value;
                    localStorage.setItem("token", token);
                    resultMsg.innerHTML = `Merci, votre jeton API a bien été récupéré.`;
                }
                else {
                    resultMsg.innerHTML = "Un souci avec la récupération de votre jeton API, êtes-vous sûr de vos identifiants / mot de passe ?";
                }
              }
        })
})
