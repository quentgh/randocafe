from django.urls import path, re_path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("profile/", views.profile, name="profile"),
    path("done/", views.done, name="done"),
    path("trips/", views.TripListView.as_view(), name="trips"),
    re_path(r"^trip/(?P<pk>\d+)$", views.TripDetailView.as_view(), name="trip-detail"),
    path("trip/create/", views.TripCreate.as_view(), name="trip-create"),
    path("trip/<int:pk>/update/", views.TripUpdate.as_view(), name="trip-update"),
    path("trip/<int:pk>/delete/", views.tripSoftDelete, name="trip-delete"),
    path("tripinstances/", views.TripInstanceListView.as_view(), name="tripinstances"),
    path(
        "tripinstance/<uuid:pk>",
        views.TripInstanceDetailView.as_view(),
        name="tripinstance-detail",
    ),
    path(
        "tripinstance/create/",
        views.TripInstanceCreate.as_view(),
        name="tripinstance-create",
    ),
    path(
        "tripinstance/<uuid:pk>/update/",
        views.TripInstanceUpdate.as_view(),
        name="tripinstance-update",
    ),
    path(
        "tripinstance/<uuid:pk>/delete/",
        views.TripInstanceDelete.as_view(),
        name="tripinstance-delete",
    ),
    path("mytrips/", views.BookedTripByUserListView.as_view(), name="mytrips"),
    path(
        "managedtrips/",
        views.AllManagedTripsListView.as_view(),
        name="managed-trips",
    ),
    path(
        "pendingtrips/",
        views.PendingTripsListView.as_view(),
        name="pending-trips",
    ),
    path(
        "inprogresstrips/",
        views.InProgressTripsListView.as_view(),
        name="inprogress-trips",
    ),
    path(
        "bookedtrips/",
        views.BookedTripsListView.as_view(),
        name="booked-trips",
    ),
    path(
        "archivedtrips/",
        views.ArchivedTripsListView.as_view(),
        name="archived-trips",
    ),
    path(
        "trip/<uuid:pk>/extend/",
        views.extend_trip_employee,
        name="extend-trip-employee",
    ),
    path("clients/", views.ClientListView.as_view(), name="clients"),
    path("client/<uuid:pk>", views.ClientDetailView.as_view(), name="client-detail"),
    path("client/create/", views.ClientCreate.as_view(), name="client-create"),
    path(
        "client/<uuid:pk>/update/", views.ClientUpdate.as_view(), name="client-update"
    ),
    path(
        "client/<uuid:pk>/delete/", views.ClientDelete.as_view(), name="client-delete"
    ),
    path("hikings/", views.HikingListView.as_view(), name="hikings"),
    re_path(
        r"^hiking/(?P<pk>\d+)$", views.HikingDetailView.as_view(), name="hiking-detail"
    ),
    path("hiking/create/", views.HikingCreate.as_view(), name="hiking-create"),
    path("hiking/<int:pk>/update/", views.HikingUpdate.as_view(), name="hiking-update"),
    path("hiking/<int:pk>/delete/", views.HikingDelete.as_view(), name="hiking-delete"),
    path("roasters/", views.RoasterListView.as_view(), name="roasters"),
    path("roaster/<uuid:pk>", views.RoasterDetailView.as_view(), name="roaster-detail"),
    path(
        "roaster/create/",
        views.RoasterCreate.as_view(),
        name="roaster-create",
    ),
    path(
        "roaster/<uuid:pk>/update/",
        views.RoasterUpdate.as_view(),
        name="roaster-update",
    ),
    path(
        "roaster/<uuid:pk>/delete/",
        views.RoasterDelete.as_view(),
        name="roaster-delete",
    ),
    path("map/", views.MarkersMapView.as_view(), name="map"),
    path("markers/", views.MarkerListView.as_view(), name="markers"),
    re_path(
        r"^marker/(?P<pk>\d+)$", views.MarkerDetailView.as_view(), name="marker-detail"
    ),
    # path("apiregister/", views.APIRegisterView.as_view(), name='api-register'),
    path(
        "token/obtain/",
        views.EmailTokenObtainPairView.as_view(),
        name="token_obtain_pair",
    ),
    # path("token/refresh/", TokenRefreshView.as_view(), name="token-refresh"),
]
