# Rando Cafe

A Web application for corporate users within a travel agency.

With the Rando Cafe service, agency employees can manage trips and reservation for the agency clients.
Each trip represent the combination of a hiking trail and a local roaster (a business partner of the agency).
Agency clients can then discover a piece a nature, a local craftsman and enjoy a selected coffee.

The concept is very similar to existing wine ou beer tour.

It's a personal project with fictional clients and user needs but they help me to write specifications. Then, I was able to start design and development works.

Here there are expected features for the Web application :
- have access to informations about the agency assets (clients, trips, hikings, business partners...)
- manage these assets
- booked a reservation
- follow the reservation status
- have access to the reservation history
- authenticated staff only
- responsive interface
- french Web application


## Technical stack

- Python 3.10
- Django 4.2.6
- GeoDjango modules
    - to install GeoDjango and other required geosoftwares :
        - https://docs.djangoproject.com/en/4.2/ref/contrib/gis/install/
        - https://docs.djangoproject.com/en/4.2/ref/contrib/gis/install/geolibs/

    - with PostgreSQL version : 14.10
    - and PostGIS extension 3.2.0

- Django REST Framework 3.14 + GIS extension (to build a local REST API to manage geo datas)
- mkcert (https protocol in local environment only)

See the requirements txt files (x2) for more details about the prerequisites.


## Specifications - Diagrams (UML)

Here we have the Use case diagram, to see actors and their interactions with the Rando Cafe application (english version coming soon).

<img src="docs/img/diag-use-case-randocafe-v0-3.png" width="640" height="" alt="Rando Cafe service - Use case diagram">

## Screenshots

### Sign in page

<img src="docs/img/00_sc_login_johndoe.png" width="640" height="" alt="Rando Cafe service - Sign in page with the John Doe user">


### Profile pages

<img src="docs/img/01_sc_profile_johndoe.png" width="640" height="" alt="Rando Cafe service - Profile page with the John Doe account">

### Home page

<img src="docs/img/10_sc_home_create_resa.png" width="640" height="" alt="Rando Cafe service - Home page">

### Trips list page

<img src="docs/img/02_sc_list_trip.png" width="640" height="" alt="Rando Cafe service - Trips list page">

### Trip edition form

<img src="docs/img/06_sc_update_trip_form.png" width="640" height="" alt="Rando Cafe service - Trip edition form">

### Trip details page with a first reservation

<img src="docs/img/13_sc_trip_detail_avec_resa_booked.png" width="640" height="" alt="Rando Cafe service - Trip details page with a reservation">

### Associated hiking page

<img src="docs/img/08_sc_hkg_paris_NE.png" width="640" height="" alt="Rando Cafe service - Hiking page of Paris east-north">

### Associated roaster page

<img src="docs/img/09_sc_roaster_belleville.png" width="640" height="" alt="Rando Cafe service - Roaster page of Paris east-north">

### Reservation page for this trip

<img src="docs/img/12_sc_resa_detail.png" width="640" height="" alt="Rando Cafe service - Reservation page for the Paris trip">

### Map with the availble hikings (Ipad view)

<img src="docs/img/sc-ipad-map-hiking.png" width="440" height="" alt="Rando Cafe service - Map with the hikings - Ipad view">

### Hiking details page (Mobile view)

<img src="docs/img/sc-andro-detail-hinking-bretagne.png" width="360" height="" alt="Rando Cafe service - Hiking details page - Mobile view">

### Trips list page (Mobile view)

<img src="docs/img/sc-andro-list-trip.png" width="360" height="" alt="Rando Cafe service - Trips list page - Mobile view">


## Next steps (maybe)

- Add tests (unit, integration)
- Add documentation (Sphinx)
- Improve security
- Improve design (accessibility)
- Add more features (geo features, add pictures, blog section...)
- Multi language Website
